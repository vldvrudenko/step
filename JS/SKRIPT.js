"use strict";
let tabtitle = document.querySelectorAll(".block-buttoms");
let content = document.querySelectorAll(".setting");
let target;

tabtitle.forEach(function (tabclick) {
  tabclick.addEventListener("click", (e) => {
    tabtitle.forEach((tabclick) => {
      tabclick.classList.remove("active");
    });
    tabclick.classList.add("active");
    target = tabclick.getAttribute("data-target");
    changecontent(target);
  });
});

function changecontent(target) {
  content.forEach((e) => {
    if (e.classList.contains(target)) {
      e.classList.add("active");
    } else e.classList.remove("active");
  });
}

let amTitle = document.querySelectorAll(".service ");
let amContent = document.querySelectorAll(".service-photo-stack");
let amTarget;

amTitle.forEach(function (amclick) {
  amclick.addEventListener("click", (e) => {
    amTitle.forEach((amclick) => {
      amclick.classList.remove("active");
    });
    amclick.classList.add("active");
    amTarget = amclick.getAttribute("data-target");
    changeAmContent(amTarget);
  });
});

function changeAmContent(amTarget) {
  amContent.forEach((e) => {
    if (e.classList.contains(amTarget)) {
      e.classList.add("active");
    } else e.classList.remove("active");
  });
}

let aboutTitle = document.querySelectorAll(".slider-line");
let aboutContent = document.querySelectorAll(".block-section");
let aboutTarget;

aboutTitle.forEach(function (aboutclick) {
  aboutclick.addEventListener("click", (e) => {
    aboutTitle.forEach((aboutclick) => {
      aboutclick.classList.remove("active");
    });
    aboutclick.classList.add("active");
    aboutTarget = aboutclick.getAttribute("data-target");
    changeAboutContent(aboutTarget);
  });
});

function changeAboutContent(aboutTarget) {
  aboutContent.forEach((e) => {
    if (e.classList.contains(aboutTarget)) {
      e.classList.add("active");
    } else e.classList.remove("active");
  });
}

const slides = document.querySelectorAll(".list-block .slider-line");
let currentSlide = 0;
const btnBack = document.getElementById("go-back");
btnBack.addEventListener("click", back);
const btnNext = document.getElementById("go-next");
btnNext.addEventListener("click", next);

function back() {
  slides[currentSlide].className = "slider-line";
  if (currentSlide == 0) {
    currentSlide = 3;
    slides[currentSlide].className = "slider-line active";
  } else {
    currentSlide = (currentSlide - 1) % slides.length;
    slides[currentSlide].className = "slider-line active";
  }
}

function next() {
  slides[currentSlide].className = "slider-line";
  currentSlide = (currentSlide + 1) % slides.length;
  slides[currentSlide].className = "slider-line active";
}
